<?php
declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Domain\Model\Product;

class DoctrineProduct
{
    private int $id;
    private float $priceWithIva;
    public function __construct(private string $name, private float $price, private string $description)
    {
    }

    public static function createFromDomainProduct(Product $product): self
    {
        return new self($product->name(), $product->price(), $product->description());
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function name(): string
    {
        return $this->name;
    }


    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function priceWithIva(): float
    {
        return $this->priceWithIva;
    }

    public function setPriceWithIva(float $priceWithIva): void
    {
        $this->priceWithIva = $priceWithIva;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}