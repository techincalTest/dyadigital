<?php
declare(strict_types=1);

namespace App\Infrastructure\Controller\Product;

use App\Application\Product\CreateProduct;
use App\Application\Product\DTO\CreateProductInputDTO;
use App\Infrastructure\Service\Request\RequestService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateProductController extends AbstractController
{
    public function __construct(private readonly CreateProduct $createProduct)
    {
    }

    /**
     * @throws \JsonException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $ouputDto = $this->createProduct->__invoke(CreateProductInputDTO::create(
            RequestService::getField($request, 'name'),
            RequestService::getField($request, 'price'),
            RequestService::getField($request, 'description'),
            RequestService::getField($request, 'iva'),
        ));
        return new JsonResponse(
            [
                'id' => $ouputDto->id,
                'name' => $ouputDto->name,
                'price' => $ouputDto->price,
                'description' => $ouputDto->description,
                'priceWithIva' => $ouputDto->priceWithIva
            ],
            Response::HTTP_CREATED
        );
    }
}