<?php
declare(strict_types=1);

namespace App\Infrastructure\Controller\Product;

use App\Application\Product\DTO\ListProductInputDTO;
use App\Application\Product\DTO\ListProductOuputDTO;
use App\Application\Product\ListProduct;
use App\Infrastructure\Service\Request\RequestService;
use Symfony\Component\HttpFoundation\Request;

class ListProductController
{
    public function __construct(private readonly ListProduct $listProduct)
    {
    }

    /**
     * @throws \JsonException
     */
    public function __invoke(Request $request): array
    {
        /*$d = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        return new JsonResponse([], Response::HTTP_OK);*/
        return $this->listProduct->__invoke(ListProductInputDTO::getAllProduct(
            RequestService::getField($request, 'name'),
            RequestService::getField($request, 'start'),
            RequestService::getField($request, 'end')
        ));
    }
}