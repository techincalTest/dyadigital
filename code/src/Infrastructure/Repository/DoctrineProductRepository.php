<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Product;
use App\Domain\Repository\ProductRepository;
use App\Infrastructure\Entity\DoctrineProduct;


class DoctrineProductRepository extends BaseRepository implements ProductRepository
{

    protected static function entityClass(): string
    {
        return DoctrineProduct::class;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(Product $product): Product
    {
        $doctrineProduct = DoctrineProduct::createFromDomainProduct($product);
        $doctrineProduct->setPriceWithIva($product->priceWithIva());
        $this->saveEntity($doctrineProduct);
        $product->setId($doctrineProduct->getId());
        return $product;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function findAllProduct($name, $start = 0, $end = 10): array
    {
        $params = [
            ':name' => $this->connection->quote($name)
        ];
        $filtros = '';

        if($name){
            $filtros = ' WHERE p.name like :name ';
        }
        $limit = "LIMIT $start, $end";

        $query = 'SELECT * FROM product p '. $filtros . $limit;
        return $this->executeFetchQuery(\strtr($query, $params));
    }

}
