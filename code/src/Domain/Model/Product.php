<?php
declare(strict_types=1);

namespace App\Domain\Model;

class Product
{
    private float $priceWithIva;
    private int $id;

    public function __construct(private string $name, private float $price, private string $description, private readonly int $iva)
    {
    }

    public function id(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function name(): string
    {
        return $this->name;
    }


    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function price(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function priceWithIva(): float
    {
        return $this->priceWithIva = ($this->price + (($this->price * $this->iva) / 100));
//        return ($this->price + (($this->price * $this->iva) / 100));
    }

    public function setPriceWithIva(float $priceWithIva): void
    {
        $this->priceWithIva = $priceWithIva;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}