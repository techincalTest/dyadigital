<?php
declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Model\Product;

interface ProductRepository
{
    public function save(Product $product): Product;
    public function findAllProduct($name, $start, $end): array;
}