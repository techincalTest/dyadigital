<?php
declare(strict_types=1);

namespace App\Application\Product;

use App\Application\Product\DTO\ListProductInputDTO;
use App\Domain\Repository\ProductRepository;

class ListProduct
{
    public function __construct(private readonly ProductRepository $productRepository)
    {
    }

    public function __invoke(ListProductInputDTO $listDto): array
    {
        return $this->productRepository->findAllProduct($listDto->name, $listDto->start, $listDto->end);
    }
}