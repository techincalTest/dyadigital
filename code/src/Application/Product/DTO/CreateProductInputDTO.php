<?php
declare(strict_types=1);

namespace App\Application\Product\DTO;

class CreateProductInputDTO
{
    private function __construct(public readonly string $name, public readonly float $price, public readonly string $description, public readonly int $iva)
    {
    }

    public static function create(string $name, float $price, string $description, int $iva): self
    {
        return new self($name, $price, $description, $iva);
    }

}