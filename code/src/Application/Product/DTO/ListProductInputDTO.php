<?php
declare(strict_types=1);

namespace App\Application\Product\DTO;

class ListProductInputDTO
{
    public function __construct(public readonly string $name, public readonly int $start, public readonly int $end)
    {
    }

    public static function getAllProduct(string $name, int $start, int $end): self
    {
        return new self($name, $start, $end);
    }
}