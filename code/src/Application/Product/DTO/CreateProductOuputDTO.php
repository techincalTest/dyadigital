<?php
declare(strict_types=1);

namespace App\Application\Product\DTO;

class CreateProductOuputDTO
{
    public function __construct(public readonly int $id, public string $name, public float $price, public string $description, public readonly float $priceWithIva)
    {
    }
}