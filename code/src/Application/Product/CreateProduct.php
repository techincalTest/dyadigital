<?php
declare(strict_types=1);

namespace App\Application\Product;

use App\Application\Product\DTO\CreateProductInputDTO;
use App\Application\Product\DTO\CreateProductOuputDTO;
use App\Domain\Model\Product;
use App\Domain\Repository\ProductRepository;

class CreateProduct
{
    public function __construct(private readonly ProductRepository $productRepository)
    {
    }

    public function __invoke(CreateProductInputDTO $productDto): CreateProductOuputDTO
    {
        $product = new Product($productDto->name, $productDto->price, $productDto->description, $productDto->iva);
        $this->productRepository->save($product);
        return new CreateProductOuputDTO($product->id(), $product->name(), $product->price(), $product->description(), $product->priceWithIva());
    }
}