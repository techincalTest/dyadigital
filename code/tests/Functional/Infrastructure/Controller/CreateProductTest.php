<?php
declare(strict_types=1);

namespace App\Tests\Functional\Infrastructure\Controller;
use App\Tests\Functional\TestBase;
use Symfony\Component\HttpFoundation\Response;

class CreateProductTest extends TestBase
{
    public function testCreateProduct(): void
    {
        $payload = [
            'name' => 'Saas',
            'price' => 30.00,
            'description' => "Hola esta es la descripcion de un producto",
            'iva' => 10,
        ];

        self::$client->request('POST', '/api/products/create', [], [], ['HTTP_Authorization' => 'admintoken'], \json_encode($payload, JSON_THROW_ON_ERROR));
        $response = self::$client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }
}